function setColor(){
    const colors = ['violet', 'peachpuff', 'lightcoral', 'cyan', 'azure', 'deepskyblue', 'darkturquoise']
    const color = colors[getRandomNumber(colors.length)]
    backgroundElement.style = `background-color:${color}`
    colorTextElement.innerText = color
}


btnColorElement.addEventListener('click', setColor)